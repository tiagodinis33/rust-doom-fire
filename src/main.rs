mod algorithm;
mod render;

use crate::algorithm::{Fire, FIRE_SIZE};
use crate::render::WINDOW_SCALAR;
use raylib::prelude::*;

fn main() {
    let (mut rl, thread) = raylib::init()
        .size(
            FIRE_SIZE as i32 * WINDOW_SCALAR,
            FIRE_SIZE as i32 * WINDOW_SCALAR,
        )
        .title("Hello, World")
        .build();
    rl.set_target_fps(60);
    let mut fire = Fire::new();
    fire.set_light_source(36);
    while !rl.window_should_close() {
        fire.run_generation();
        let mut draw_context = rl.begin_drawing(&thread);
        draw_context.clear_background(Color::WHITE);
        render::render_fire(&fire, &mut draw_context);
        draw_context.draw_fps(10, 10);
    }
}
