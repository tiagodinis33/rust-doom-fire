use crate::algorithm::{Fire, FIRE_SIZE};
use raylib::prelude::{RaylibDraw, RaylibDrawHandle};
pub mod colors;
pub const WINDOW_SCALAR: i32 = 5;
pub fn render_fire(fire: &Fire, d: &mut RaylibDrawHandle) {
    for x in 0..FIRE_SIZE as i32 {
        for y in 0..FIRE_SIZE as i32 {
            let color = colors::COLORS[fire.pixels[((y * FIRE_SIZE as i32) + x) as usize] as usize];
            d.draw_rectangle(
                x * WINDOW_SCALAR,
                y * WINDOW_SCALAR,
                WINDOW_SCALAR,
                WINDOW_SCALAR,
                color,
            );
        }
    }
}
