use rand::Rng;
use raylib::get_random_value;
use std::cmp::min;
pub const FIRE_SIZE: usize = 100;
pub struct Fire {
    // The last row is reserved for the light source
    // So we add 1 to the column size
    pub pixels: [u8; FIRE_SIZE * (FIRE_SIZE + 1)],
}
impl Fire {
    pub fn new() -> Self {
        return Self {
            pixels: [0; FIRE_SIZE * (FIRE_SIZE + 1)],
        };
    }
    pub fn set_light_source(&mut self, intensity: u8) {
        for x in 0..FIRE_SIZE {
            self.pixels[(FIRE_SIZE * (FIRE_SIZE - 1)) + x] = intensity;
        }
    }
    pub fn run_generation(&mut self) {
        let mut rng = rand::thread_rng();
        for i in 0..(FIRE_SIZE * (FIRE_SIZE - 1)) {
            let decay = rng.gen::<u8>() % 3;
            let below_pixel_fire_intensity = self.pixels[i + FIRE_SIZE];
            let new_fire_intensity = if decay <= below_pixel_fire_intensity {
                below_pixel_fire_intensity - decay
            } else {
                0
            };
            let new_i = if decay as usize <= i {
                i - decay as usize
            } else {
                0
            };
            self.pixels[new_i] = new_fire_intensity;
        }
    }
}
